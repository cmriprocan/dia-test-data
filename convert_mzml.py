import argparse
import os
import shutil
import sys
import glob
from subprocess import check_call

import docker

import pandas as pd

from tqdm import tqdm


def main():
    parser = argparse.ArgumentParser(
        description='Convert the raw data',
    )
    parser.add_argument(
        'method',
        type=str,
        help='The conversion method (e.g. sciex, toffee)',
    )
    args = parser.parse_args()

    if args.method == 'sciex':
        wiff_files = sorted(glob.glob(os.getcwd() + '/**/*.wiff', recursive=True))
        for wiff in tqdm(wiff_files):
            mzml_docker(wiff)
    elif args.method == 'toffee':
        tof_docker()
    else:
        raise ValueError(f'Invalid method: {args.method}')


def mzml_docker(wiff_file):
    base_outdir = os.path.dirname(wiff_file).rstrip('/wiff')
    name = os.path.splitext(os.path.basename(wiff_file))[0]
    mzml_file = wiff_file.replace('wiff', 'mzML')
    if os.path.exists(mzml_file):
        return

    volumes = {}

    def add_volume(d, docker_d):
        if d not in volumes:
            volumes[d] = {'bind': docker_d, 'mode': 'rw'}
        return volumes[d]['bind']

    wiff_docker_dir = add_volume(f'{base_outdir}/wiff', '/wiff')
    mzml_dir = f'{base_outdir}/mzML'
    mzml_docker_dir = add_volume(mzml_dir, '/mzML')

    docker_client = docker.from_env()
    docker_image = 'sciex/wiffconverter:0.9'

    command = [
        'mono',
        '/usr/local/bin/sciex/wiffconverter/OneOmics.WiffConverter.exe',
        'WIFF',
        f'{wiff_docker_dir}/{name}.wiff',
        '-profile',
        'MZML',
        f'{mzml_docker_dir}/{name}.mzML',
        '--zlib',
        '--index',
    ]
    command = ' '.join(command)

    container = docker_client.containers.run(
        image=docker_image,
        command=command,
        volumes=volumes,
        detach=True,
    )
    exit_status_dict = container.wait()
    exit_status = exit_status_dict['StatusCode']
    output = container.logs(
        stdout=True,
        stderr=True,
        stream=False,
    ).decode('utf-8')
    container.remove(force=True)

    if exit_status != 0:
        raise docker.errors.ContainerError(
            container,
            exit_status,
            command,
            docker_image,
            output,
        )


def tof_docker(base_outdir, scratch_outdir, metadata):
    raise NotImplementedError("")
    import docker

    volumes = {}

    def add_volume(d, docker_d):
        if d not in volumes:
            volumes[d] = {'bind': docker_d, 'mode': 'rw'}
        return volumes[d]['bind']

    mzml_docker_dir = add_volume(f'{base_outdir}/mzML', '/mzML')
    tof_scratch_basedir = f'{scratch_outdir}/tof'
    tof_docker_dir = add_volume(tof_scratch_basedir, '/tof')

    docker_client = docker.from_env()
    docker_image = 'cmriprocan/openms-toffee:0.13.9.dev'

    # ---
    # mzML to Toffee
    mzml_to_tof_keys = ['a', 'e']  # msconvert + sciex profile
    for i, row in metadata.iterrows():
        for key in mzml_to_tof_keys:
            print(i, 'of', metadata.shape[0], '||', row.InjectionName, ' || mzml_to_toffee ||', key)

            # make output directories
            tof_scratch_dir = f'{tof_scratch_basedir}/{key}'
            tof_final_dir = f'{base_outdir}/tof/{key}'
            os.makedirs(tof_scratch_dir, exist_ok=True)
            os.makedirs(tof_final_dir, exist_ok=True)

            tof_basename = f'{row.InjectionName}.{key}.tof'
            tof_err_basename = f'{row.InjectionName}.{key}.err.h5'
            tof_spec_basename = f'{row.InjectionName}.{key}.spech5'
            command = [
                'mzml_to_toffee',
            ]
            if i == 0:
                command += [
                    '-keep_cache_file',
                    '--mass_accuracy_path',
                    f'{tof_docker_dir}/{key}/{tof_err_basename}',
                ]
            command += [
                f'{mzml_docker_dir}/{key}/{row.InjectionName}.{key}.mzML',
                f'{tof_docker_dir}/{key}/{tof_basename}',
            ]
            command = ' '.join(command)

            tof_scratch_path = f'{tof_scratch_dir}/{tof_basename}'
            if os.path.exists(tof_scratch_path):
                continue

            container = docker_client.containers.run(
                image=docker_image,
                command=command,
                volumes=volumes,
                detach=True,
            )
            exit_status_dict = container.wait()
            exit_status = exit_status_dict['StatusCode']
            output = container.logs(
                stdout=True,
                stderr=True,
                stream=False,
            ).decode('utf-8')
            container.remove(force=True)

            if exit_status != 0:
                raise docker.errors.ContainerError(
                    container,
                    exit_status,
                    command,
                    docker_image,
                    output,
                )

            shutil.move(
                f'{tof_scratch_dir}/{tof_basename}',
                f'{tof_final_dir}/{tof_basename}',
            )
            if os.path.isfile(f'{tof_scratch_dir}/{tof_err_basename}'):
                shutil.move(
                    f'{tof_scratch_dir}/{tof_err_basename}',
                    f'{tof_final_dir}/{tof_err_basename}',
                )
            if os.path.isfile(f'{tof_scratch_dir}/{tof_spec_basename}'):
                shutil.move(
                    f'{tof_scratch_dir}/{tof_spec_basename}',
                    f'{tof_final_dir}/{tof_spec_basename}',
                )


if __name__ == '__main__':
    main()

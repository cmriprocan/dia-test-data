Data downloaded from ftp://PASS00289:XY4524h@ftp.peptideatlas.org

SRL generated using docker image `openswath/openswath:0.1.2` and the following:

```bash
TargetedFileConverter -in OpenSWATH_SM4_GoldStandardAssayLibrary.TraML -out OpenSWATH_SM4_GoldStandardAssayLibrary.tsv
TargetedFileConverter -in OpenSWATH_SM4_GoldStandardAssayLibrary.TraML -out OpenSWATH_SM4_GoldStandardAssayLibrary.pqp
TargetedFileConverter -in OpenSWATH_SM4_iRT_AssayLibrary.TraML -out OpenSWATH_SM4_iRT_AssayLibrary.tsv
TargetedFileConverter -in OpenSWATH_SM4_iRT_AssayLibrary.TraML -out OpenSWATH_SM4_iRT_AssayLibrary.pqp
```

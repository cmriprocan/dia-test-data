import matplotlib.pyplot as plt
import pandas as pd


simple_df = pd.read_csv('hek-top2000.sciex.txt', sep='\t')
complex_df = pd.read_csv('ecoli.sciex.txt', sep='\t')

simple_df['PrecursorId'] = simple_df.modification_sequence + '_' + simple_df.prec_z.map(str)
complex_df['PrecursorId'] = complex_df.modification_sequence + '_' + complex_df.prec_z.map(str)

fig, ax = plt.subplots(figsize=(10, 10))
merged = pd.merge(simple_df, complex_df, on='PrecursorId')
ax.plot(merged.iRT_x, merged.iRT_y, 'bo', ms=15)
ax.plot(merged.RT_detected_x, merged.RT_detected_y, 'rv', ms=15)
ax.plot([5, 70], [5, 70], '--', alpha=0.5)
plt.show()

s = set(simple_df.PrecursorId.unique())
c = set(complex_df.PrecursorId.unique())
common = s.intersection(c)
not_in_c = s.difference(c)
print('Number of PrecursorId in common:', len(common))
print('Number of PrecursorId in simple but not complex:', len(not_in_c))

c_plus_s_df = pd.concat([
    complex_df,
    simple_df.loc[simple_df.PrecursorId.isin(not_in_c)],
])
assert not c_plus_s_df.iRT.isna().any()
c_plus_s_df.to_csv('ecoli.hek-top2000.sciex.txt', sep='\t', index=False)


import os
import sys

import pandas as pd

import toffee
from toffee import util
from toffee.manipulation import Subsampler


def main():
    srl_fname = '../srl/hek-top2000.with_decoys.tsv'
    srl = pd.read_csv(srl_fname, sep='\t')
    irt_fname = '../srl/hek-top2000.iRT.tsv'
    irt = pd.read_csv(irt_fname, sep='\t')
    precursor_and_product_df = pd.concat((srl, irt)).drop_duplicates(subset=['PrecursorMz', 'ProductMz'])

    if len(sys.argv) == 1:
        dilutions = [1, 2, 4]
    else:
        dilutions = list(map(int, sys.argv[1:]))

    for dilution in dilutions:
        background = 'ecoli'
        tof_sample_key = f'in-silico-dilution.procan90-m06-07.{background}.{dilution:02d}'
        sampler = Subsampler(
            tof_fname=f'{tof_sample_key}.tof',
            precursor_and_product_df=precursor_and_product_df,
            ppm_width=100,
        )
        sampler.run(f'{tof_sample_key}-subsampled.tof')


if __name__ == '__main__':
    main()


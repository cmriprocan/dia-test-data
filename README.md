# dia-test-data

A git LFS repo for tracking large test files useful in developing DIA pipelines (primarily SWATH-MS). 

Git submodules are pretty painful with private repos, so instead, the recommended usage is to have this repo checked out and to create an environment variable that references its location: `export DIA_TEST_DATA_REPO=${HOME}/Develop/dia-test-data`.

## Adding a new file

- First make sure that `git lfs` is installed

```
sudo apt install git-lfs
git lfs install
```

- Then track the file; we choose to be explict about the files we track, rather than using wildcards to prevent accidental inclusion of huge files.

```
git lfs track <filename>
```

- Then add the file and use git as you normally would

```
git add <filename>
git commit -m "Your message"
```
